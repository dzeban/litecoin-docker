litecoin-docker
---------------

Docker image build and deploy to kubernetes for litecoin daemon. Everything is
tied using Gitlab CI, so check the .gitlab-ci.yml first.

Dockerfile is inspired by
[uphold/docker-litecoin-core](https://github.com/uphold/docker-litecoin-core/blob/master/0.18/Dockerfile) though I've simplified it a bit. 
In the future I would simplify it even more like this:

* Created builder image - debian with curl, gpg and keys imported to avoid
  installing it every time during build time like it's done now.
* I would also tried to make a static build of litecoind or build it with musl
  libc so we can use alpine image for the final base. Now `debian:11-slim` is
  used but it has its overhead.

On each commit the CI pipeline will build docker image and push into [Gitlab
Container Registry](https://gitlab.com/dzeban/litecoin-docker/container_registry). 
It will be tagged depending on the branch/tag:

* Commits on master will be pushed without docker tag, so it can be pulled as
  `latest`
* Commits on branches will be pushed with branch name as docker tag. Branch
  named `some-branch-1.1` will be pushed as `litecoind:some-branch-1-1`.
* Commits on tags will be pushed with git tag as docker tag, e.g. git tag
  `0.18.1` is pushed as `litecoind:0.18.1`

Example builds:

* on [master branch](https://gitlab.com/dzeban/litecoin-docker/-/jobs/1841814409)
* on [tag](https://gitlab.com/dzeban/litecoin-docker/-/jobs/1839045783)

Deploy is performed only on master branch or on tags. Deploy is performed via
button click. We use Kubernetes as a target platform. Currently, deploy is
confugured with Kubernetes cluster in GKE. Credentials and other cluster related
information is set in Gitlab CI variables accessible only on protected branches
and tags.

Example deploy [on tag 0.18.1](https://gitlab.com/dzeban/litecoin-docker/-/jobs/1841293765).

Litecoind will be deployed as StatefulSet with a persistent volume for data.

Kubernetes deployment is in `deploy` directory. It's templated with Helm for
multiple environments (currently only staging).
