# Dockerfile to build image with litecoind.
# Inspired by https://github.com/uphold/docker-litecoin-core/blob/master/0.18/Dockerfile

FROM debian:11 as builder

# Version can be passed via --build-arg VERSION=0.17.1, by default 0.18.1 is used
ARG VERSION=0.18.1

LABEL maintainer="alex@dzyoba.com"
LABEL org.label-schema.vcs-url="https://gitlab.com/dzeban/litecoin-docker"

# Use bash instead of POSIX shell for error handling options (pipefail)
SHELL ["/bin/bash", "-c"]

# GPG public key that was used to sign .asc file with sha256 hashes
ENV GPG_KEY=59caf0e96f23f53747945fd4fe3348877809386c

# Download, verify and unpack litecoin release in a single RUN command to avoid
# creating multiple layers.
RUN set -euxo pipefail \
  && apt update -y \
  && apt install -y curl gnupg \
  && gpg --no-tty --keyserver keyserver.ubuntu.com --recv-keys ${GPG_KEY} \
  && curl -sSLO https://download.litecoin.org/litecoin-${VERSION}/linux/litecoin-${VERSION}-x86_64-linux-gnu.tar.gz \
  && curl -sSLO https://download.litecoin.org/litecoin-${VERSION}/linux/litecoin-${VERSION}-linux-signatures.asc \
  && gpg --verify litecoin-${VERSION}-linux-signatures.asc \
  && grep $(sha256sum litecoin-${VERSION}-x86_64-linux-gnu.tar.gz | cut -f1 -d' ') litecoin-${VERSION}-linux-signatures.asc \
  && tar -C / -xf *.tar.gz

# Copy single binary from previous stage for minimal image.
# Unfortunately, litecoind release build is not built with musl libc so alpine
# cannot be used.
FROM debian:11-slim
ARG VERSION=0.18.1
RUN useradd -m litecoin -u 1000
USER litecoin
COPY --from=builder /litecoin-${VERSION}/bin/litecoind /usr/local/bin/
ENTRYPOINT ["litecoind"]
